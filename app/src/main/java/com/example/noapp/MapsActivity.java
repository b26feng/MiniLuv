package com.example.noapp;

import android.Manifest;
import android.app.ActivityManager;
import android.app.usage.UsageStats;
import android.app.usage.UsageStatsManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Looper;
import android.os.PersistableBundle;
import android.provider.Settings;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.core.app.NavUtils;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import com.example.noapp.background.BigBrother;
import com.google.android.gms.common.internal.Constants;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.navigation.NavigationView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;

import androidx.lifecycle.Observer;
import androidx.navigation.NavController;
import androidx.navigation.NavDestination;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.work.Constraints;
import androidx.work.Data;
import androidx.work.OneTimeWorkRequest;
import androidx.work.Operation;
import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkInfo;
import androidx.work.WorkManager;

import static java.lang.Long.min;
import static java.util.jar.Pack200.Unpacker.PROGRESS;

// import com.google.android.gms.location.FusedLocationProviderApi;

public class MapsActivity extends AppCompatActivity implements
        OnMapReadyCallback,
        GoogleMap.OnCameraMoveStartedListener,
        GoogleMap.OnCameraIdleListener,
        GoogleMap.OnMapClickListener,
        GoogleMap.OnMarkerClickListener,
        LocationListener {

    private GoogleMap mMap;
    private FusedLocationProviderClient fusedLocationClient;
    private Location last_loc;

    private AppBarConfiguration mAppBarConfiguration;


    private static final String TAG = MapsActivity.class.getSimpleName();

    private Map<Marker,Circle> zones;
    private List<Marker> centers;
    private LocationRequest locationRequest;
    private boolean reqeustLocationUpdates = true;

    private LocationCallback locationCallback;

    private OneTimeWorkRequest oBrien = null;
    private boolean watching_state = false;
    private int current_block_time = 0;
    private int current_duration = 20;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //usageAccessSettingsPage();

        restoreState(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_maps);
        Toolbar toolbar = findViewById(R.id.toolbar);
        DrawerLayout drawer = findViewById(R.id.android_mobile___1);
        setSupportActionBar(toolbar);

        NavigationView navigationView = findViewById(R.id.nav_view);
        // setSupportActionBar(topbar);

        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home)
                .setDrawerLayout(drawer)
                .build();
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        //NavigationUI.setupWithNavController(navigationView, navController);


        zones = new LinkedHashMap<Marker, Circle>();
        centers = new ArrayList<Marker>();


        createLocationRequest();

        // Create Location Callback
        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                // Finding the nearest
                Location most_recent = last_loc;
                long time_stamp = 0;
                for (Location location : locationResult.getLocations()) {
                    if(time_stamp < location.getTime()) {
                        time_stamp = location.getTime();
                        most_recent = location;
                    }
                }
                last_loc = most_recent;

                // Check if need to block app
                if(isInsideAny()) {
                    if(oBrien == null) {
                        bigBrotherIsWatchingYou();
                    }
                }else{
                    screensOff();
                }
            };
        };



        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        fusedLocationClient.getLastLocation()
                .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        if(location != null){
                            last_loc = location;
                            LatLng cur_loc = new LatLng(last_loc.getLatitude(),last_loc.getLongitude());

                            //mMap.moveCamera(CameraUpdateFactory.zoomIn());
                            mMap.moveCamera(CameraUpdateFactory.newLatLng(cur_loc));

                        }
                    }
                });
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.moveCamera(CameraUpdateFactory.zoomBy(8));
        mMap.setOnMapClickListener(this);
        mMap.setOnCameraMoveStartedListener(this);
        mMap.setMyLocationEnabled(true);
        mMap.setOnMarkerClickListener(this);

        mMap.setMinZoomPreference(8.0f);
        mMap.setMaxZoomPreference(20.0f);



        boolean success = googleMap.setMapStyle(new MapStyleOptions(getResources()
                .getString(R.string.style_json)));
        if (!success) {
            Log.e(TAG, "Style parsing failed." + getString(R.string.style_json).length());
        }
    }



    @Override
    public void onMapClick(LatLng point) {
        //mTapTextView.setText("tapped, point=" + point);
        //Toast.makeText(this,"tapped point = "+point,Toast.LENGTH_SHORT).show();
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        final View pop_up_view = inflater.inflate(R.layout.add_zone_popup,null);
        Button btn_cancel = pop_up_view.findViewById(R.id.btn_add_zone_cancel_view);
        Button btn_ok = pop_up_view.findViewById(R.id.btn_add_zone_ok_view);


        // create the popup window
        int width = LinearLayout.LayoutParams.WRAP_CONTENT;
        int height = LinearLayout.LayoutParams.WRAP_CONTENT;
        boolean focusable = true; // lets taps outside the popup also dismiss it
        final PopupWindow popupWindow = new PopupWindow(pop_up_view, width, height, focusable);
        popupWindow.setAnimationStyle(R.style.Theme_AppCompat_Dialog);

        popupWindow.showAtLocation(findViewById(R.id.android_mobile___1), Gravity.CENTER, 0, -200);



        // dismiss the popup window when cancel
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button
                popupWindow.dismiss();
            }
        });

        final LatLng f_point = point;
        btn_ok.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                TextView rad_input = pop_up_view.findViewById(R.id.rad_input);
                TextView duration_input = pop_up_view.findViewById(R.id.duration_input);
                current_duration = Integer.parseInt(duration_input.getText().toString());
                String s_radius = rad_input.getText().toString();
                int i_radius = Integer.parseInt(s_radius);
                addNoAppZone(f_point,i_radius);
                popupWindow.dismiss();
                // Code here executes on main thread after user presses button
            }
        });





        //if(centers.get(0)!=null) centers.get(0).remove();
    }

    @Override
    public void onCameraIdle() {

    }

    @Override
    public void onCameraMoveStarted(int reason) {
        switch (reason) {
            case GoogleMap.OnCameraMoveStartedListener
                .REASON_DEVELOPER_ANIMATION:
                Toast.makeText(this,"The app moved the camera",Toast.LENGTH_SHORT).show();
            break;
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        if(location != null){
            last_loc = location;
            LatLng cur_loc = new LatLng(last_loc.getLatitude(),last_loc.getLongitude());
            mMap.addMarker(new MarkerOptions().position(cur_loc).title("Marker in Sydney"));
            mMap.moveCamera(CameraUpdateFactory.newLatLng(cur_loc));
            if(isInsideAny()) {
                if(oBrien == null) {
                    bigBrotherIsWatchingYou();
                }
            }else{
                screensOff();
            }
        }
    }

    @Override
    public boolean onMarkerClick(Marker marker) {


        // Check if I'm allowed to remove a marker
        if(last_loc!=null && marker!=null) {
            if (isInside(marker) && watching_state) {
                Toast.makeText(this, "You are not allowed to delete the NoApp Zone you are in during the blocking time. Unblocks after "+current_block_time+"secs" , Toast.LENGTH_LONG).show();

                return true;
            }
        }

        // Create Popup window
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        final View pop_up_view = inflater.inflate(R.layout.delete_zone_popup,null);
        Button btn_cancel = pop_up_view.findViewById(R.id.btn_delete_zone_cancel_view);
        Button btn_ok = pop_up_view.findViewById(R.id.btn_delete_zone_ok_view);

        // create the popup window
        int width = LinearLayout.LayoutParams.WRAP_CONTENT;
        int height = LinearLayout.LayoutParams.WRAP_CONTENT;
        boolean focusable = true; // lets taps outside the popup also dismiss it
        final PopupWindow popupWindow = new PopupWindow(pop_up_view, width, height, focusable);
        popupWindow.setAnimationStyle(R.style.Theme_AppCompat_Dialog);

        popupWindow.showAtLocation(findViewById(R.id.android_mobile___1), Gravity.CENTER, 0, -200);



        // dismiss the popup window when cancel
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button
                popupWindow.dismiss();
            }
        });

        final Marker f_marker = marker;
        btn_ok.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                deleteNoAppZone(f_marker);
                popupWindow.dismiss();
                // Code here executes on main thread after user presses button
            }
        });



        return true;
    }



    private void addNoAppZone(LatLng pos, double radius){
        Marker m = mMap.addMarker(new MarkerOptions().position(pos).title("NoApp Zone"));
        Circle c = mMap.addCircle(new CircleOptions().center(pos).radius(radius).strokeColor(Color.RED).fillColor(0X44FF0000));
        zones.put(m,c);
        centers.add(m);

        // Check if I'm in the zone, if I'm in create oBrien
        if(isInside(m)){
            bigBrotherIsWatchingYou();
        }
    }

    private void deleteNoAppZone(Marker marker){
        Circle c = zones.get(marker);
        if(c!=null){
            c.remove();
            marker.remove();
            zones.remove(marker);
            centers.remove(marker);
        }

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_main_drawer, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(reqeustLocationUpdates){
            startLocationUpdates();
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        saveState(outState);
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        restoreState(savedInstanceState);
    }

    private void startLocationUpdates() {
        fusedLocationClient.requestLocationUpdates(locationRequest,locationCallback, Looper.getMainLooper());
    }

    protected void createLocationRequest(){
        locationRequest = LocationRequest.create();
        locationRequest.setInterval(1000);
        locationRequest.setFastestInterval(500);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);


        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);

        SettingsClient client = LocationServices.getSettingsClient(this);
        //Task<LocationSettingsResponse> task = client.checkLocationSettings(builder.build());

        /*
        // Create a location settinsg request builder
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest); // mLocationRequestHighAccuracy
        //.addLocationRequest(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY); // mLocationRequestBalancedPowerAccuracy
        //builder.setNeedBle(true);

         */
    }



    private void bigBrotherIsWatchingYou(){

        // Get Main Page name
        ActivityManager mActivityManager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        ActivityManager.RunningTaskInfo ar= mActivityManager.getRunningTasks(1).get(0);
        ComponentName top = ar.topActivity;
        String map_name = top.getClassName();

        Data inputData = new Data.Builder().putInt("D",current_duration).putString("M",map_name).build();

        OneTimeWorkRequest bigBrother = new OneTimeWorkRequest.Builder(BigBrother.class)
                .setInputData(inputData)
                //.setInitialDelay(, TimeUnit.SECONDS)
                .build();
        WorkManager.getInstance(getApplicationContext()).enqueue(bigBrother);
        oBrien = bigBrother;
        watching_state = true;
        WorkManager.getInstance(getApplicationContext()).getWorkInfoByIdLiveData(oBrien.getId())
                .observe(this,new Observer<WorkInfo>(){
                    @Override
                    public void onChanged(WorkInfo workInfo) {
                        if(workInfo == null || workInfo.getState() == WorkInfo.State.SUCCEEDED){
                            watching_state = false;
                            setTimerDisplay(0);
                        }
                    }
                });

        // Set Progress Update
        WorkManager.getInstance(getApplicationContext())
                // requestId is the WorkRequest id
                .getWorkInfoByIdLiveData(oBrien.getId())
                .observe(this, new Observer<WorkInfo>() {
                    @Override
                    public void onChanged(@Nullable WorkInfo workInfo) {
                        if (workInfo != null) {
                            Data progress = workInfo.getProgress();
                            int value = progress.getInt(PROGRESS, 0);
                            current_block_time = value;
                            // Do something with progress
                            setTimerDisplay(value);
                        }
                    }
                });
    }

    private boolean isInsideAny(){
        if(zones.size() < 1 ) return false;
        float[] dist = new float[1];
        for(int i = 0;i<centers.size();i++){
            Marker marker = centers.get(i);
            Location.distanceBetween(last_loc.getLatitude(), last_loc.getLongitude(), marker.getPosition().latitude, marker.getPosition().longitude, dist);
            if(dist[0] < zones.get(marker).getRadius()) return true;
        }
        return false;
    }

    private boolean isInside(Marker marker){
        if(marker==null) return false;
        float[] dist = new float[1];
        Location.distanceBetween(last_loc.getLatitude(), last_loc.getLongitude(), marker.getPosition().latitude, marker.getPosition().longitude, dist);
        return(dist[0] < zones.get(marker).getRadius());
    }

    private void setTimerDisplay(int i){
        TextView timer = findViewById(R.id.menu_timer);
        LinearLayout timer_group = findViewById(R.id.timer_group);
        TextView unblock_after = findViewById(R.id.unblock_after);
        if(i > 0) {
            unblock_after.setText("Unblock After");
            timer_group.setVisibility(View.VISIBLE);
            if(timer == null){
                Log.e("setTimerDisplay:", "timer is null");
            }
            timer.setText(" "+i);

        }else if (i==0){
            unblock_after.setText("Unblocked");
            timer_group.setVisibility(View.INVISIBLE);
        }
    }

    private void screensOff(){
        if(oBrien!=null) {
            WorkManager.getInstance().cancelAllWork();
            oBrien =  null;
        }
    }

    private void saveState(Bundle bundle){
        if(bundle!=null){
            bundle.putInt("CCount",zones.size());
            // Save the keys
            Marker[] keys = new Marker[zones.size()];
            zones.keySet().toArray(keys);
            for(int i = 0; i<zones.size();i++) {
                bundle.putDouble("Lng"+i,keys[i].getPosition().longitude);
                bundle.putDouble("Lat"+i,keys[i].getPosition().latitude);
                bundle.putDouble("C"+i,zones.get(keys[i]).getRadius());
            }

            // Save watching state
            bundle.putBoolean("WS",watching_state);

            // Save
            bundle.putInt("cbt",current_block_time);
            bundle.putInt("cd",current_duration);
        }

    }

    private void restoreState(Bundle bundle){
        // If there's saved data, get them
        if(bundle!=null) {
            int len = bundle.getInt("CCount");
            for (int i = 0; i < len; i++) {
                double lng = bundle.getDouble("Lng" + i);
                double lat = bundle.getDouble("Lat" + i);
                double radius = bundle.getDouble("C" + i);
                LatLng pos = new LatLng(lat, lng);
                addNoAppZone(pos, radius);
            }

            watching_state = bundle.getBoolean("WS");
            current_duration = bundle.getInt("cd");
            current_block_time  = bundle.getInt("cbt");
        }



    }

}
