package com.example.noapp.ui.blocked;

import android.app.Activity;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Icon;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.noapp.R;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class BlockedFragment extends Fragment {

    private BlockedViewModel blockedViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        Activity cur_act = getActivity();
        PackageManager packageManager = cur_act.getPackageManager();
        List<PackageInfo> packs = packageManager.getInstalledPackages(PackageManager.GET_ACTIVITIES);

        blockedViewModel =
                ViewModelProviders.of(this).get(BlockedViewModel.class);
        View root = inflater.inflate(R.layout.fragment_blocked, container, false);

        int[] tids = {R.id.text_blocked,R.id.text_blocked1,R.id.text_blocked2,R.id.text_blocked3};

        int[] pids = {0,1,2,3,13,5,31,93,86,186,202,204};

        int[] iids = {R.id.icon_blocked,R.id.icon_blocked1,R.id.icon_blocked2,R.id.icon_blocked3,
                R.id.icon_blocked5,R.id.icon_blocked6,R.id.icon_blocked7,R.id.icon_blocked8,
                R.id.icon_blocked10,R.id.icon_blocked16,R.id.icon_blocked17,R.id.icon_blocked18};


        for(int i = 0; i<12;i++){
            PackageInfo p = packs.get(pids[i]);


            //final TextView textView = root.findViewById(tids[i]);
            final ImageView imageView = root.findViewById(iids[i]);
            Drawable appIcon;
            try {
                appIcon = packageManager.getApplicationIcon(p.packageName);
                imageView.setImageDrawable(appIcon);
                //textView.setText(p.packageName);
            } catch (PackageManager.NameNotFoundException e) {
                TextView textView = root.findViewById(tids[0]);
                textView.setText("Fuck!");
            }

        /*blockedViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });*/
        }
        return root;
    }
}