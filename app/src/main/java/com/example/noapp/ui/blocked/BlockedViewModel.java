package com.example.noapp.ui.blocked;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class BlockedViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public BlockedViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("Tips: You are not allowed to remove a\n NoApp Zone when you are inside it.");
    }

    public LiveData<String> getText() {
        return mText;
    }
}