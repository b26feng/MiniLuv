package com.example.noapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

public class NoShowActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_no_show);
    }

    @Override
    protected void onResume() {
        super.onResume();
        finish();
    }
}
