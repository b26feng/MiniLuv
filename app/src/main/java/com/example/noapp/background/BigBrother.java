package com.example.noapp.background;

import android.app.ActivityManager;
import android.app.usage.UsageStats;
import android.app.usage.UsageStatsManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.work.Data;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.example.noapp.BlockedActivity;
import com.example.noapp.NoShowActivity;

import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.xml.transform.Result;

import static java.util.jar.Pack200.Unpacker.PROGRESS;

public class BigBrother extends Worker {
    private Context mContext;
    private long secs = 20;
    private String map_activity;
    private String block_activity = "com.example.noapp.BlockedActivity";
    private String noshow_activity = "com.example.noapp.NoShowActivity";
    private boolean is_new = true;
    public BigBrother(
            @NonNull Context context,
            @NonNull WorkerParameters params) {
        super(context, params);
        mContext = context;
        secs = getInputData().getInt("D",20);
        map_activity = getInputData().getString("M");
        setProgressAsync(new Data.Builder().putInt(PROGRESS, (int)secs).build());
    }

    @Override
    public Result doWork() {
        // Do the work here--in this case, upload the images.

        //getForegroundApp();


        for(long i = 0; i< secs; i++) {

            try {
                findForegroundApp();
                Thread.sleep(1000);
                UpdateProgress(i+1);
            }catch (InterruptedException e){
                break;
            }
        }

        // Indicate whether the task finished successfully with the Result
        return Result.success();
    }

    public String getForegroundApp() {
        String currentApp = "NULL";

        final Context m_context = getApplicationContext();
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            UsageStatsManager usm = (UsageStatsManager) m_context.getSystemService(Context.USAGE_STATS_SERVICE);
            long time = System.currentTimeMillis();
            List<UsageStats> appList = usm.queryUsageStats(UsageStatsManager.INTERVAL_DAILY, time - 1000 * 1000, time);
            if (appList != null && appList.size() > 0) {
                SortedMap<Long, UsageStats> mySortedMap = new TreeMap<Long, UsageStats>();
                for (UsageStats usageStats : appList) {
                    mySortedMap.put(usageStats.getLastTimeUsed(), usageStats);
                }
                if (mySortedMap != null && !mySortedMap.isEmpty()) {
                    currentApp = mySortedMap.get(mySortedMap.lastKey()).getPackageName();
                }
            }
        } else {
            ActivityManager am = (ActivityManager) m_context.getSystemService(Context.ACTIVITY_SERVICE);
            List<ActivityManager.RunningAppProcessInfo> tasks = am.getRunningAppProcesses();
            currentApp = tasks.get(0).processName;
        }
        Log.e("BigBrother:", "Current App in foreground is: " + currentApp);
        Looper.loop();
        return currentApp;
    }

    public String findForegroundApp(){
        Context m_context = getApplicationContext();
        ActivityManager mActivityManager = (ActivityManager) m_context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> RunningTask = mActivityManager.getRunningTasks(2);
        ActivityManager.RunningTaskInfo ar = RunningTask.get(0);
        ComponentName top = ar.topActivity;
        String cur_app = top.getClassName();
        if(!(map_activity.equals(cur_app)||block_activity.equals(cur_app) || noshow_activity.equals(cur_app))) {
            Log.e("BigBrother:", "Current App in foreground is: " + cur_app);
            Intent blockScreen;
            if(is_new) {
                blockScreen = new Intent(mContext, BlockedActivity.class);
                blockScreen.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                is_new = false;
            }else{
                blockScreen = new Intent(mContext, NoShowActivity.class);
                blockScreen.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
            }
            mContext.startActivity(blockScreen);
        }
        return cur_app;
    }

    private void UpdateProgress(long count){
        setProgressAsync(new Data.Builder().putInt(PROGRESS, (int)(secs-count)).build());
    }

}