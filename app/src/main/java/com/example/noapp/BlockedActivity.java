package com.example.noapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class BlockedActivity extends AppCompatActivity {
    Context mContext;
    boolean pop = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blocked);
        mContext = getApplicationContext();
    }

    public void backToMaps(View view) {
        finish();
        /*
        Intent blockScreen = new Intent(mContext, MapsActivity.class);
        //blockScreen.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY|Intent.FLAG_ACTIVITY_NEW_TASK);
        blockScreen.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT|Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_NO_HISTORY);
        mContext.startActivity(blockScreen);
        */

    }


    /*
    @Override
    protected void onResume() {
        super.onResume();
        if(pop) {
            finish();
        }else{
            pop = true;
        }
    }
    */

}
